from flask import Flask, request
from flask.ext.cors import CORS, cross_origin
import json

app = Flask(__name__)
#cors = CORS(app, resources={r"/employee/*": {"origins": "*"}})
app.config['CORS_HEADERS'] = 'Content-Type'
cors = CORS(app, resources={r"/employee/*": {"origins": "http://localhost:80"}})

"""@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
    return response"""

data = [
    {
        "id": 1001,
        "empName": "Balyan,Ankit",
        "skill": "Java",
        "designation": "Analyst",
        "department": "Sogeti",
        "location": "Newyork"
    },
    {
        "id": 1002,
        "empName": "Solanki,Satish",
        "skill": "Python",
        "designation": "Consultant",
        "department": "Sogeti",
        "location": "Mumbai"
    },
    {
        "id": 1003,
        "empName": "Kant,Ripu",
        "skill": "AngularJS",
        "designation": "Consultant",
        "department": "AppsOne","location":"Newyork"
    },
    {
        "id": 1004,
        "empName": "Chouhan,Arpit",
        "skill": "Flask",
        "designation": "Consultant",
        "department": "AppsTwo",
        "location": "Pune"
    },
    {
        "id": 1005,
        "empName": "Bhansali,Sanjay",
        "skill": "Java",
        "designation": "Consultant",
        "department": "Sogeti",
        "location": "Singapore"
     },
    ]


@app.route('/employee/getAll', methods=['GET','OPTIONS'])
@cross_origin(origin='localhost',headers=['Content- Type', 'Authorization'])
def get_all():
    return json.dumps(data)


@app.route('/employee/getEmployee/<int:id>', methods=['GET'])
def get_employee(id):
    for name in data:
        if str(name['id']) == str(id):
            print("inside if\n")
            return json.dumps(name)
    return json.dumps({"status": "False"})


@app.route('/employee/createEmployee', methods=['GET', 'POST'])
def create_employee():
    if request.method == 'POST':
        data.append(request.get_json(force=True))
        return json.dumps({"status": "True"})
    else:
        return json.dumps({"status": "False"})


@app.route('/employee/updateEmployee', methods=['GET', 'PUT'])
def update_employee():
    if request.method == 'PUT':
        d = request.get_json(force=True)
        for name in data:
            print(name, d)
            if int(name['id']) == int(d['id']):
                print("INSIDE IF")
                name['empName'] = d['empName']
                name['skill'] = d['skill']
                name['designation'] = d['designation']
                name['department'] = d['department']
                name['location'] = d['location']
                return json.dumps({"status": "True"})
    return json.dumps({"status": "False"})


@app.route('/employee/deleteEmployee/<int:emp_id>', methods=['GET', 'DELETE'])
def delete_employee(emp_id):
    if request.method == 'DELETE':
        for e in range(len(data)):
            if str(data[e]['id']) == str(emp_id):
                data.pop(e)
                return json.dumps({"status": "True"})
        return json.dumps({"status": "False"})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
